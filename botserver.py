import asyncio
import os
import requests
import time
from datetime import datetime
import re
import random
import json
import sys
import urllib
from urllib import parse
import io
from collections import defaultdict
import overpass
from nio import (AsyncClient, RoomMessageText, InviteMemberEvent, ClientConfig, LoginResponse, UploadResponse,
                 ProfileSetAvatarResponse, DeleteDevicesError, DeleteDevicesResponse, RoomLeaveError)
import aiofiles.os
from PIL import Image
import magic
from geopy import distance
from geopy.geocoders.nominatim import Nominatim

import persistence
import rss as feed
import hangman
from data import download_data, publish_data

client = None
logged_in = None
image_cache = {}

def add_thread_id(content, thread_id):
    if thread_id:
        content.update({
            "m.relates_to": {
                "rel_type": "m.thread",
                "event_id": thread_id
            }
        })

async def send_message(room_id, message, thread_id=None, type='m.text'):
    content = {
        "msgtype": type,
        "body": message
    }
    add_thread_id(content, thread_id)
    await client.room_send(
        room_id=room_id,
        message_type="m.room.message",
        content=content
    )
async def send_emote(room_id, message):
    await send_message(room_id, message, type='m.emote')

async def send_notice(room_id, message):
    await send_message(room_id, message, type='m.notice')

async def send_image(room_id, image, thread_id):
    image_path, file_size, mime_type, width, height, content_uri = image
    content = {
        "body": os.path.basename(image_path),  # descriptive title
        "info": {
            "size": file_size,
            "mimetype": mime_type,
            "thumbnail_info": None,  # TODO
            "w": width,  # width in pixel
            "h": height,  # height in pixel
            "thumbnail_url": None,  # TODO
        },
        "msgtype": "m.image",
        "url": content_uri,
    }
    add_thread_id(content, thread_id)

    try:
        await client.room_send(
            room_id,
            message_type="m.room.message",
            content=content
        )
        print("Image was sent successfully")
    except Exception:
        print(f"Image send of file {image} failed.")

async def send_formatted_message(room_id, message, thread_id=None):
    content = {
        "msgtype": "m.text",
        "format": "org.matrix.custom.html",
        "body": message,
        "formatted_body": message
    }
    add_thread_id(content, thread_id)
    await client.room_send(
        room_id=room_id,
        message_type="m.room.message",
        content=content
    )

async def send_or_replace(room_id, message):
    async def last_bot_message():
        resp = await client.sync(timeout=10000, full_state=True) # get next batch token
        messages = await client.room_messages(room_id, start=resp.next_batch, limit=2)
        if messages.chunk:
            last_message = messages.chunk[1] # msg before command or bot response
            if last_message.sender == os.environ['MATRIX_USER']:
                is_edit = 'm.relates_to' in last_message.source['content']
                return last_message.source['content']['m.relates_to']['event_id'] if is_edit else last_message.event_id

    async def send_replace(room_id, event_id, message):
        new_message = {
            "m.new_content": {
                "msgtype": "m.text",
                "format": "org.matrix.custom.html",
                "body": message,
                "formatted_body": message
            },
            "m.relates_to": {
                "rel_type": "m.replace",
                "event_id": event_id
            },
            "msgtype": "m.text",
            "format": "org.matrix.custom.html",
            "body": message,
            "formatted_body": message
        }
        resp = await client.room_send(
            room_id=room_id,
            message_type="m.room.message",
            content=new_message
        )
    bot_message_id = await last_bot_message()
    if bot_message_id:
        await send_replace(room_id, bot_message_id, message)
    else:
        await send_formatted_message(room_id, message)

async def echo(room_id, event_body, thread_id):
    await send_message(room_id, event_body.replace('!echo', ''), thread_id)

async def weather(room_id, event_body, thread_id):
    location = event_body
    location = location if location else 'Nuremberg'
    response = requests.get(f'https://wttr.in/{location}?format=3').text
    await send_message(room_id, response, thread_id)

def commits():
    url = 'https://gitlab.com/app.lab/bot/-/commits/master?format=atom'
    feed = requests.get(url).text
    id_regex = '<id>(.*?)</id>'
    title_regex = '<title.*?>(.*?)</title>'
    stamp_regex = '<updated>(.*?)</updated>'
    ids = [item.rsplit('/', 1)[1][0:11] for item in re.findall(id_regex, feed, re.DOTALL) if 'master' not in item]
    titles = [item.strip() for item in re.findall(title_regex, feed, re.DOTALL) if 'bot:master' not in item]
    stamps = [item.strip() for item in re.findall(stamp_regex, feed, re.DOTALL)]
    stamps = stamps[1:] # offset feed timestamp
    changes = list(zip(ids, titles, stamps))
    return changes

async def sup(room_id, event_body, thread_id):
    num = event_body
    num = int(num) if num else 5
    changes = commits()
    changes = changes if int(num) > len(changes) else changes[0:int(num)]
    formatted_changes = '\n'.join(['{:13s}{:s}'.format(id, title) for id, title, _ in changes])
    await send_formatted_message(room_id, f'<pre>{formatted_changes}\n </pre>', thread_id)

async def help(room_id, event_body, thread_id):
    keys = commands.keys()
    keys = f'[{", ".join(keys)}]'
    help = [
        f'Say one of {keys} and I\'ll answer!',
        f'At your service via {keys}.',
        f'You say {keys} and I say something else.',
    ]
    rand = random.randint(0, len(help)-1)
    await send_message(room_id, help[rand], thread_id)

async def chess(room_id, event_body, thread_id):
    game_type = event_body

    url = 'https://lichess.org/setup/friend'
    # &mode=1 == rated, requires auth
    base = 'variant=1&fen=&time=5.0&time_range=9&days=2&days_range=2&color=random'
    blitz = f'{base}&timeMode=1&increment=0&increment_range=0'
    unlimited = f'{base}&timeMode=0&increment=8&increment_range=8&mode=0'
    mode = unlimited
    if 'blitz' in game_type:
        mode = blitz
    elif 'help' in game_type:
        return await send_message(room_id, 'choose one of `!chess blitz` or `!chess unlimited` or just `!chess`, unlimited is default!', thread_id)

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0',
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    response = requests.post(url, data=mode, headers=headers)
    await send_message(room_id, response.url, thread_id)

async def note(room_id, event_body, thread_id):
    parts = event_body
    if ' ' in parts:
        cmd, remainder = parts.split(' ', 1)
    else:
        cmd = parts
        remainder = ''
    message = ''
    if 'add' in cmd:
        if ' ' in remainder:
            scope, text = remainder.split(' ', 1)
            persistence.add_note(text, scope)
        else:
            text = remainder
            persistence.add_note(text)
        message = 'note added'
    if 'list' in cmd:
        if remainder:
            if ' ' in remainder:
                scope, all = remainder.split(' ')
            else:
                scope, all = remainder, False
            notes = persistence.read_notes(scope)
            notes = notes if all else notes[:6]
            message = 'notes\n-----\n'
            message += '\n'.join(['{:<5d}{:20s}{:30s}'.format(id, scope, text) for id, scope, text in notes])
            message = f'<pre>{message}\n </pre>'
        else:
            scopes = persistence.list_notes()
            message = 'scopes\n------\n'
            message += '\n'.join(['{:30s}{:<5d}'.format(scope, count) for scope, count in scopes])
            message = f'<pre>{message}\n </pre>'
    if 'del' in cmd:
        id = remainder.strip()
        persistence.delete_note(id)
        message = f'note {id} deleted'
    if not cmd or 'help' in cmd:
        message = """Usages: 
            !note list (&lt;scope&gt;)           # Shows last 6 notes in descending order
            !note list (&lt;scope&gt;) all       # Shows all notes in descending order
            !note add (&lt;scope&gt;) text       # Adds a note to an optional scope (default scope is 'default')
            !note del &lt;id&gt;                 # Deletes note by a given id
        """
        message = f'<pre>{message}\n </pre>'
    await send_formatted_message(room_id, message, thread_id)

async def find(room_id, event_body, thread_id):
    keywords = event_body
    api = overpass.API()
    city, amenity = '', ''
    if ' ' in keywords:
        city, amenity = keywords.split(' ')
    if city and amenity:
        query = f"""
            area["name"='{city}']->.searchArea;
            (node(area.searchArea)[amenity='{amenity}'];);
        """
        fmt = 'csv("name","addr:postcode","addr:city","addr:street","addr:housenumber","website")'
        data = api.get(query, responseformat=fmt)
        header, *rows = data
        rows = ['\t'.join(header)] + ['\t'.join(row) for row in rows]
        message = '\n'.join(rows)
        message = f'<pre>{message}\n</pre>' if message else "No results"
    else:
        message = 'Usage: !find &lt;city&gt; &lt;amenity&gt;'

    await send_formatted_message(room_id, message, thread_id)

def colorize(message):
    def color():
        colors = [
            ('#F0F8FF', '#00308F'),
            ('#FBFFE6', '#FF7E00'),
            ('#FFEEE6', '#800000'),
            ('#F2FFE6', '#3B7A57')
        ]
        r = random.randint(0, len(colors)-1)
        return colors[r]
    back, text = color()
    return f'<span data-mx-bg-color="{back}" data-mx-color="{text}">{message}</span>'

def format(entries):
    if entries and len(entries[0]) == 4:
        fmt_entries = [f'<span data-mx-color="#CCCCCC">{source}</span> '
                       + colorize(f'<b>&nbsp;&nbsp;&nbsp;<a href="{url}">{title}</a></b>')
                       for id, title, url, source in entries]
    else:
        fmt_entries = [colorize(f'<b>{id}&nbsp;&nbsp;&nbsp;&nbsp;<a href="{url}">{title}</a></b>') for id, title, url in entries]
    return '<br/>'.join(fmt_entries)

async def rss(room_id, event_body, thread_id):
    parts = event_body
    message = """<pre>
        Usage
        -----
        !rss add &lt;url&gt;         # add a rss or atom feed
        !rss list              # list available rss feeds (including ids)
        !rss del &lt;id&gt;          # remove rss feed via id (and subscriptions)
        !rss show &lt;id&gt;         # list entries of a given feed
        !rss show latest       # show the latest entries of all feeds
        !rss sub               # show subscriptions of current room
        !rss sub &lt;id&gt;          # subscribe to a feed via id
        !rss unsub &lt;id&gt;        # unsubscribe from a feed
        !rss                   # show this usage description
                        </pre>
                        """
    if not parts:
        pass
    else:
        cmd, *params = parts.split(' ', 1)

        if 'add' in cmd:
            if params and params[0]:
                url = params[0]
                title = feed.extract_title(url)
                id = persistence.add_rss(url, title)
                await feed.update_rss(url)
                message = f'feed [<b>{title}</b>] with id [<b>{id}</b>] added.'
        if 'list' in cmd:
            feeds = persistence.list_rss()
            message = format(feeds)
        if 'del' in cmd:
            if params and params[0]:
                id = params[0]
                persistence.delete_rss(id)
                message = f'feed [<b>{id}</b>] deleted.'
        if 'show' in cmd:
            if params and params[0]:
                if 'latest' in params[0]:
                    message = format(list(feed.latest()))
                else:
                    rss_id = params[0]
                    entries = persistence.list_entries(rss_id)
                    message = format([(id, title, url) for id, title, url, ts, source in entries])
                    message = message if entries else 'no entries.'
        if 'sub' in cmd:
            if params and params[0]:
                rss_id = params[0]
                # event.source['content']['m.relates.to']['event_id']
                #                                        ['rel_type'] == 'm.thread'
                room_name = client.rooms[room_id].display_name
                if thread_id:
                    persistence.add_sub(room_id, rss_id, thread_id)
                    message = f'feed [<b>{rss_id}</b>] added to thread [<b>{thread_id}</b>] in room [<b>{room_name}</b>]'
                else:
                    persistence.add_sub(room_id, rss_id)
                    message = f'feed [<b>{rss_id}</b>] added to room [<b>{room_name}</b>]'
            else:
                subs = [(sub[0], sub[1], sub[2]) for sub in persistence.list_subs(room_id)]
                message = format(subs)
                message = message if subs else 'no subs.'

        if 'unsub' in cmd:
            if params and params[0]:
                rss_id = params[0]
                persistence.del_sub(room_id, rss_id, thread_id)
                room_name = client.rooms[room_id].display_name
                if thread_id:
                    message = f'feed [<b>{rss_id}</b>] removed from thread [<b>{thread_id}</b>] in room [<b>{room_name}</b>]'
                else:
                    message = f'feed [<b>{rss_id}</b>] removed from room [<b>{room_name}</b>]'


    await send_formatted_message(room_id, message or '', thread_id)

async def hm(room_id, event_body, thread_id): #fixme
    letter = event_body
    if letter:
        response = hangman.play(letter)
    else:
        response = hangman.start()
    await send_or_replace(room_id, f'<pre>{response} \n</pre>')

async def data_cmd(room_id, event_body, thread_id):
    parts = event_body
    message = """usage: 
                    !data &lt;url&gt;          # download
                    !data publish &lt;url&gt;  # download and publish
                    !data publish              # publish downloads
    """
    message = f'<pre>{message}</pre>'
    if parts:
        cmd, *params = parts.split(' ', 1)
        if 'publish' in cmd:
            if params and params[0]:
                url = params[0]
                target_url = publish_data(url)  # dl and publish
            else:
                target_url = publish_data()
            message = f'data available at <a href="{target_url}">{target_url}</a>'
        elif 'http' in cmd:
            url = cmd
            target_url = download_data(url)
            message = f'data downloaded to <a href="{target_url}">{target_url}</a>'
    await send_formatted_message(room_id, message, thread_id)

async def nutrients(room_id, event_body, thread_id):
    def parse_headers(reqheads):
        res = dict()
        for line in reqheads.split('\n'):
            k,v = line.split(': ')
            k,v = k.strip(), v.strip()
            # k,v = bytes(k, 'utf8').decode('utf-8'), str(v)
            res[k] = v
        return res

    food_name = event_body
    message = """usage: 
                    !nut &lt;name&gt;          # look up nutrients for food name
    """
    message = f'<pre>{message}</pre>'
    if food_name:
        reqheads = """accept: application/json, text/plain, */*
        content-type: application/json
        origin: https://www.nutritionix.com
        referer: https://www.nutritionix.com/
        user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36"""
        url = 'https://www.nutritionix.com/track-api/v2/natural/nutrients'
        data = f"""{{"query": "{food_name}"}}"""
        res = requests.post(url, data=data, headers=parse_headers(reqheads))
        j = json.loads(res.text)
        if 'foods' in j and j['foods']:
            nuts = j['foods'][0]
            keys = ['food_name', 'nf_calories', 'nf_total_fat', 'nf_saturated_fat', 'nf_sodium', 'nf_total_carbohydrate',
                    'nf_sugars','nf_protein']
            values = ''
            for key in keys:
                values += '{:<30}{:>10}\n'.format(key, nuts[key])
            message = f'<pre>{values}</pre>'
    await send_formatted_message(room_id, message, thread_id)

async def url_to_file(url):
    res = requests.get(url)
    folder = 'temp'
    if not os.path.exists(folder):
        await aiofiles.os.mkdir(folder)
    file_name = url.split('/')[-1]
    file = f'{folder}/{file_name}_{time.time()}'
    with open(file, 'wb') as f:
        f.write(res.content)
    return file

async def upload(room_id, image_path, thread_id):
    mime_type = magic.from_file(image_path, mime=True)  # e.g. "image/jpeg"
    if not mime_type.startswith("image/") and room_id:
        await send_formatted_message(room_id, "Not an image.", thread_id) # fixme move to outer function?
    im = Image.open(image_path)
    (width, height) = im.size

    # first do an upload of image, then send URI of upload to room
    file_stat = await aiofiles.os.stat(image_path)
    file_size = file_stat.st_size
    async with aiofiles.open(image_path, "r+b") as f:
        resp, maybe_keys = await client.upload(
            f,
            content_type=mime_type,  # image/jpeg
            filename=os.path.basename(image_path),
            filesize=file_stat.st_size)
    if (isinstance(resp, UploadResponse)):
        print("Image was uploaded successfully to server. ")
    else:
        print(f"Failed to upload image. Failure response: {resp}")
    return image_path, file_size, mime_type, width, height, resp.content_uri

async def image(room_id, event_body, thread_id):
    image_url = event_body
    message = """usage: 
                    !image &lt;url&gt;          # display image url
    """
    message = f'<pre>{message}</pre>'
    if image_url:
        image_path = None
        try:
            if image_url in image_cache:
                image = image_cache[image_url]
            else:
                image_path = await url_to_file(image_url)
                image = await upload(room_id, image_path, thread_id)
                image_cache[image_url] = image
            await send_image(room_id, image, thread_id)
        finally:
            if image_path:
                await aiofiles.os.remove(image_path)
    else:
        await send_formatted_message(room_id, message, thread_id)

async def distance_cmd(room_id, event_body, thread_id):
    def read_germanies_cities():
        url = 'https://en.wikipedia.org/wiki/List_of_cities_and_towns_in_Germany'
        from lxml import etree
        from io import BytesIO
        text = requests.get(url).text
        xml = etree.parse(BytesIO(text.encode('UTF-8')))
        xpath = '//*/table/tbody/tr/td/ul/li/a[1]/text()'
        for el in xml.xpath(xpath):
            yield str(el)

    def dist(src, tgt):
        return int(distance.distance(src, tgt).kilometers)

    n = Nominatim(user_agent='my_custom_ua')
    def latlng(city):
        r = n.geocode(city)
        return r.latitude, r.longitude

    def extract_distances(url):
        website = requests.get(url).text
        cities = [c for c in read_germanies_cities()]
        src = latlng('Nuernberg')
        for city in cities:
            if city in website:
                yield city, dist(src, latlng(city))
    distance_url = event_body
    message = """usage: 
            !distance &lt;url&gt;   # extracts cities from website and displays distances from nue
    """
    is_url = lambda url: parse.urlparse(url).scheme in ('http', 'https')
    if distance_url and is_url(distance_url):
        message = ''
        for city in extract_distances(distance_url):
            name, km = city
            message = message + f"{name} ({km} km)\n"
    message = f'<pre>{message}</pre>'
    await send_formatted_message(room_id, message, thread_id)

async def avatar(room_id, event_body, thread_id):
    avatar_url = event_body
    image_path = await url_to_file(avatar_url)
    try:
        image = await upload(room_id, image_path, thread_id)
    finally:
        if image_path:
            await aiofiles.os.remove(image_path)
    *_, content_uri = image
    resp = await client.set_avatar(content_uri)
    if type(resp) is ProfileSetAvatarResponse:
        message = "avatar updated."
    else:
        message = f"update of avatar failed {resp}"
    await send_message(room_id, message, thread_id)

async def leave(room_id, event_body, thread_id):
    room_name = client.rooms[room_id].display_name
    print(f'leaving room {room_name}')
    await send_message(room_id, 'goodbye!', thread_id)
    resp = await client.room_leave(room_id)
    if isinstance(resp, RoomLeaveError):
        message = f'leave failed with {resp}'
        await send_message(room_id, message, thread_id)
    else:
        print(f'successfully left room {room_name}')

async def notify():
    for sub in persistence.list_all_subs():
        entries = []
        room_id, rss_id, last_update, thread_id = sub
        for entry in persistence.list_entries(rss_id):
            id, title, url, ts, source = entry
            if not last_update or ts > last_update:
                entries.append((id, title, url, source))
        if entries:
            if thread_id:
                persistence.update_sub(room_id, rss_id, thread_id)
            else:
                persistence.update_sub(room_id, rss_id)
            await send_formatted_message(room_id, format(entries), thread_id)

async def daily_notes():
    room_id = None
    for room in client.rooms:
        if 'Unencrypted' in client.rooms[room].display_name:
            room_id = room

    cfg = persistence.config()
    last_note_id = cfg['last_note_id'] if 'last_note_id' in cfg.keys() else -1
    scope_group = defaultdict(str)
    for id, scope, text in persistence.latest_notes(last_note_id):
        if int(id) > int(last_note_id):
            last_note_id = id
        scope_group[scope] = str(int(scope_group[scope])+1) if scope_group[scope] else '1'
    notes = [colorize(f'<b>{scope}({count})</b>') for scope, count in scope_group.items()]
    message = f"new notes in: {', '.join(notes)}"
    if notes and room_id:
        persistence.config('last_note_id', last_note_id)
        await send_formatted_message(room_id, message)

async def daily_changes():
    room_id = None
    for room in client.rooms:
        if 'Unencrypted' in client.rooms[room].display_name:
            room_id = room

    cfg = persistence.config()
    last_change_date = cfg['last_change_date'] if 'last_change_date' in cfg.keys() else None

    changes = commits()
    if last_change_date:
        filter_fn = lambda commit: datetime.fromisoformat(commit[2][0:10]) > datetime.fromisoformat(last_change_date)
    else:
        filter_fn = lambda commit: datetime.fromisoformat(commit[2]).date() == datetime.today().date()
    changes = list(filter(filter_fn, changes))

    formatted_changes = '\n'.join(['{:13s}{:s}'.format(id, title) for id, title, _ in changes])
    size = len(changes)
    change_plural = f'{size} changes' if size > 1 else f'{size} change'
    have_plural = f'have' if size > 1 else f'has'
    messages = [f"{change_plural} towards total awesomeness {have_plural} been made today.",
                f"I might run even better now due to {change_plural} today.",
                f"I mutated {size} times today."]
    picked = messages[random.randint(0, len(messages)-1)]
    message = f"<details><summary>{picked} 🤖</summary><pre>{formatted_changes}\n </pre></details>"
    if changes and room_id:
        persistence.config('last_change_date', str(datetime.today())[0:10])
        await send_formatted_message(room_id, message)


commands = {
    '!echo': echo,
    '!weather': weather,
    '!sup': sup,
    '!help': help,
    '!chess': chess,
    '!note': note,
    '!find': find,
    '!rss': rss,
    '!hm': hm,
    '!data': data_cmd,
    '!nut': nutrients,
    '!img': image,
    '!distance': distance_cmd,
    '!avatar': avatar,
    '!leave': leave

}

def extract_thread_id(event):
    if 'content' in event.source \
            and 'm.relates_to' in event.source['content'] \
            and 'event_id' in event.source['content']['m.relates_to'] \
            and 'rel_type' in event.source['content']['m.relates_to'] \
            and 'm.thread' in event.source['content']['m.relates_to']['rel_type']:
        thread_id = event.source['content']['m.relates_to']['event_id']
        return thread_id
    return None

async def message_cb(room, event):
    if 'local' in os.environ['ENV']:
        print(f"Message received for room {room.display_name} | {room.user_name(event.sender)}: {event.body}")
    if logged_in and event.server_timestamp > logged_in and event.sender != os.environ['MATRIX_USER']:
        for command, method in commands.items():
            if event.body.startswith(command) or event.body.startswith(f' * {command}'): # allow for edits
                event_body = event.body.replace(f' * {command}', '').replace(command, '').strip()
                thread_id = extract_thread_id(event)
                await method(room.room_id, event_body, thread_id)

async def invite_cb(source, sender):
    if logged_in and sender.sender != os.environ['MATRIX_USER']:
        if sender.membership == 'invite':
            print(f'invite received from {sender.sender} to join {source.display_name}')
            global client
            await client.join(source.room_id)
            print(f'successfully joined {source.display_name}')

async def trust_devices(client, user_id):
    devices = client.device_store[user_id].items()
    print(f"{len(devices)} devices found for user {user_id}")
    deletable_devices = []
    for device_id, olm_device in devices:
        if user_id == client.user_id and device_id == client.device_id:
            # ignore bot's own device
            continue
        elif user_id == client.user_id:
            # collect bot's devices and delete, unlimited devices will be created since they can't be cached due to
            # herokus ephemeral file system
            deletable_devices.append(olm_device.device_id)
        else:
            client.verify_device(olm_device)
            print(f"Trusting {device_id} from user {user_id}")

    if deletable_devices:
        print(f'deleting {len(deletable_devices)} devices')
        try:
            auth = {"type": "m.login.password",
                    "user": user_id,
                    "password": os.environ['MATRIX_PASS']}
            resp = await client.delete_devices(deletable_devices, auth=auth)
            assert isinstance(resp, DeleteDevicesResponse)
            if resp is DeleteDevicesError:
                print(f'could not delete devices')
            devices = client.device_store[user_id].items()
            print(f'{len(devices)} devices left')
        except Exception as error:
            print(f'error {error} of type {type(error)}')

async def login(client):
    """ avoids creating devices by caching login sessions"""
    credentials = 'credentials.json'
    def cache_session(resp):
        with open(credentials, "w") as f:
            json.dump({
                "access_token": resp.access_token,
                "device_id": resp.device_id,
                "user_id": resp.user_id
            }, f)

    if os.path.exists(credentials):
        try:
            with open(credentials, 'r') as f:
                config = json.load(f)
                client.access_token = config['access_token']
                client.user_id = config['user_id']
                client.device_id = config['device_id']
                client.load_store()
                print(f"Logged in using stored credentials: {client.user_id} on {client.device_id}")
        except RuntimeError as err:
            print(f"Couldn't load session from file. Logging in. Error: {err}")

    if not client.user_id or not client.access_token or not client.device_id:
        resp = await client.login(os.environ['MATRIX_PASS'])

        if isinstance(resp, LoginResponse):
            print("Logged in using a password; saving details to disk")
            cache_session(resp)
        else:
            print(f"Failed to log in: {resp}")
            sys.exit(1)


async def main():
    global client
    global logged_in
    store_path = 'nio_store/'
    if not os.path.exists(store_path):
        os.mkdir(store_path)
    config = ClientConfig(store_sync_tokens=True)
    client = AsyncClient(
        homeserver=os.environ['MATRIX_HOST'],
        user=os.environ['MATRIX_USER'],
        store_path=store_path,
        config=config
    )
    client.add_event_callback(message_cb, RoomMessageText)
    client.add_event_callback(invite_cb, InviteMemberEvent)

    await login(client)

    async def sync_and_trust_devices():
        await client.synced.wait()
        rooms = await client.joined_rooms()
        rooms = rooms.rooms
        if 'local' in os.environ['ENV'] and os.environ['DEBUG_ROOM']:
            rooms = [os.environ['DEBUG_ROOM']] # speedup
        members = set()
        for room in rooms:
            if client.rooms[room].encrypted:
                response = await client.joined_members(room)
                for member in response.members:
                    members.add(member.user_id)
        for user_id in members:
            await trust_devices(client, user_id)

    sync_and_trust_devices_task = asyncio.create_task(sync_and_trust_devices())
    # full_state considers old invite events as well
    sync_forever_task = asyncio.create_task(client.sync_forever(30000, full_state=True))

    async def schedule(func, interval, *args):
        while True:
            await asyncio.sleep(interval)
            try:
                if asyncio.iscoroutinefunction(func):
                    await func(*args) if args else await func()
                else:
                    func(*args) if args else func()
            except Exception as e:
                print(e)
    schedule_task = asyncio.create_task(schedule(feed.update_rss, 1*60*60))
    notify_task = asyncio.create_task(schedule(notify, 5))
    daily_notes_task = asyncio.create_task(schedule(daily_notes, 3*60*60))
    daily_changes_task = asyncio.create_task(schedule(daily_changes, 7*60*60))
    save_task = asyncio.create_task(schedule(persistence.b2_upload, 30, persistence.bucket, persistence.filename))

    logged_in = round(time.time() * 1000)

    async def sync_data_at_startup():
        # avoid using stale data
        if os.path.exists(persistence.filename):
            os.remove(persistence.filename)
        persistence.b2_download(persistence.bucket, persistence.filename, persistence.filename)
        print('data synced')
    sync_data_at_startup_task = asyncio.create_task(sync_data_at_startup())

    await asyncio.gather(
        sync_and_trust_devices_task,
        sync_forever_task,
        sync_data_at_startup_task
    )

    await asyncio.gather(
        schedule_task,
        notify_task,
        daily_notes_task,
        daily_changes_task,
        save_task
    )

asyncio.get_event_loop().run_until_complete(main())
