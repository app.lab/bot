# bot

based on https://matrix-nio.readthedocs.io/en/docs/nio.html

# setup
- add the ssh key to gitlab project
- clone the repo
- `heroku login`
- add a heroku remote i.e. `heroku git:remote -a heroku_app_name`

```
git push origin # to gitlab
git push heroku # to heroku and restarts the dyno
```

heroku logs

## env vars

- local
create .env file with the following entries
```
MATRIX_HOST=value
MATRIX_USER=value
MATRIX_PASS=value
ENV=local
B2_KEY=value
B2_KEY_ID=value
B2_BUCKET=value
```

- heroku 
```
heroku config:set MATRIX_HOST=value
heroku config:set MATRIX_USER=value
heroku config:set MATRIX_PASS=value
heroku config:set ENV=heroku
heroku config:set B2_KEY=value
heroku config:set B2_KEY_ID=value
heroku config:set B2_BUCKET=value

```

# heroku setup
- setting up dynos require package libolm
- apt support requires a buildpack
- packages can then be added via listing them in Aptfile
```
heroku buildpacks:add --index 1 heroku-community/apt
```

# misc
- heroku's filesystem doesn't allow for caching devices in `/nio_store`
- therefore unlimited devices are created
- therefore these get deleted on bot startup 
- `JoinedRoomsError` can be resolved by deleting `credentials.json`