import json
import os
import subprocess
import glob

import requests
import heroku3

from tree2sets.graph2set import traverse_json, traverse_xml, persist_nodes
from tree2sets.html2set import parse_html, persist_matrices

def as_sqlite(url):
    def clean_url(url):
        url = url.replace('https://', '')
        for token in ['.', '/', '-']:
            url = url.replace(token, '_')
        return url

    def reset_db(database_name):
        if os.path.exists(database_name):
            # how to deal with files and versions is up to the caller
            os.remove(database_name)

    def fetch_json():
        response = requests.get(url).text
        if not '[' in response[0]:
            response = f'[{response}]'
        return clean_url(url), json.loads(response)

    def fetch_xml():
        xml = requests.get(url).text
        return clean_url(url), xml

    def fetch_html():
        html = requests.get(url).text
        return clean_url(url), html.replace('!<DOCTYPE html>', '').strip()

    json_or_xml = url.endswith('json') or url.endswith('xml')
    if json_or_xml:
        nodes, file_name, database_name = None, None, None
        if url.endswith('json'):
            file_name, j = fetch_json()

            database_name = f'{file_name}.sqlite'
            reset_db(database_name)

            nodes = traverse_json(file_name, j)

        elif url.endswith('xml'):
            file_name, xml = fetch_xml()

            database_name = f'{file_name}.sqlite'
            reset_db(database_name)

            nodes = traverse_xml(xml)

        print(nodes)
        persist_nodes(database_name, nodes) # change to database_name in package
        return database_name
    else:
        file_name, html = fetch_html()
        database_name = f'{file_name}.sqlite'
        reset_db(database_name)
        mats = parse_html(html)
        if mats:
            persist_matrices(database_name, mats)
            return database_name

def _target():
    appname = 'blooming-tundra-96984'
    target_url = f'https://{appname}.herokuapp.com'
    return appname, target_url

def download_data(url):
    database_name = as_sqlite(url)
    datasette_name = database_name.replace('.sqlite', '')
    _, target_url = _target()
    return f'{target_url}/{datasette_name}'

def publish_data(url=None):
    heroku_conn = heroku3.from_key(os.environ['HEROKU_API_KEY'])
    appname, target_url = _target()
    if appname not in heroku_conn.apps():
        app = heroku_conn.create_app(name=appname)
        print(f'app {app.name} created')
    app = heroku_conn.apps()[appname]
    command = ['heroku', 'plugins:install', 'heroku-builds']
    subprocess.run(command)
    databases = glob.glob('*.sqlite')
    command=['datasette', 'publish', 'heroku', '-n', app.name, *databases]
    print(command)
    subprocess.run(command)
    if url:
        target_url = download_data(url)
    message = f'data available at {target_url}.'
    print(message)
    return target_url

if __name__ == "__main__":
    # url = 'https://raw.githubusercontent.com/datasets/covid-19/main/datapackage.json'
    # url = 'https://raw.githubusercontent.com/datasets/country-list/master/datapackage.json'
    # download_data(url)
    # download_data('https://xkcd.com/rss.xml')
    # publish_data()
    # publish_data('https://xkcd.com/atom.xml')
    # download_data('https://en.wikipedia.org/wiki/BMW_4_Series_(F32)')
    download_data('https://en.wikipedia.org/wiki/BMW_3_Series_(E90)')