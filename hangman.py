import random
import requests
from types import SimpleNamespace

game = SimpleNamespace()

def pick_word():
    res = requests.get('https://raw.githubusercontent.com/openethereum/wordlist/master/res/wordlist.txt')
    word_list = res.text.split('\n')
    word = random.choice(word_list)
    return word.upper()

def start():
    game.word = pick_word()
    game.word_tracking = "_" * len(game.word)
    game.tries = 6
    game.guessed = False
    game.guessed_letters = []
    image = draw_hangman(game.tries)
    message = [
        "Let us play Hangman!",
        f"{image}",
        f"{game.word_tracking}",
        f"Please guess a letter by using !hm [letter]",
        "or to exit the game type !hm exit"
    ]
    message = "\n\n".join(message)
    return message

def play(guess):
    if 'guessed' in game.__dict__ and not game.guessed and game.tries > 0:
        guess = guess.upper()
        if len(guess) == 1 and guess.isalpha():
            if guess in game.guessed_letters:
                return f'You already guessed {guess} <br/> <br/> {game.word_tracking}'
            elif guess not in game.word:
                game.tries -= 1
                game.guessed_letters.append(guess)
                image = draw_hangman(game.tries)
                return f'{guess}, is not in the word. <br/> <br/> {image} <br/> <br/> {game.word_tracking}'
            else:
                game.guessed_letters.append(guess)
                word_as_list = list(game.word_tracking)
                indices = [i for i, letter in enumerate(game.word) if letter == guess]
                for index in indices:
                    word_as_list[index] = guess
                game.word_tracking = "".join(word_as_list)
                if "_" not in game.word_tracking:
                    game.guessed = True
                return f'Good job,{guess},is in the word!<br/> <br/> {game.word_tracking}'
        elif len(guess) > 1 and guess == 'EXIT':
            return f'Bye bye!'
        else:
            return f'Not a valid guess, only one letter is allowed.'
    elif 'guessed' in game.__dict__ and game.guessed:
        return f'Congrats, You win!To start a new game type !hm <br/> or to exit the game type !hm exit'
    else:
        return f'you ran out of tries. To start a new game type !hm <br/> or to exit the game type !hm exit'


def draw_hangman(tries):
    game_over = '\n\t'.join([
        f"""Game over! Sorry, you ran out of tries. The word was {game.word}.""",
        "To start a new game type !hm",
        "  or to exit the game type !hm exit"
    ])
    message = f"{tries} try left" if tries == 1 else f"{tries} tries left"
    message = game_over if tries == 0 else message

    image = SimpleNamespace()
    image.beam = "--------"
    image.rope = "|      |"
    image.head = "|      O"
    image.arms = "|     \\|/"
    image.body = "|      |"
    image.legs = "|     / \\"
    image.base = "-         "
    image.note = f"{message}\n"

    if tries == 6:
        image = SimpleNamespace()
        image.note = f"{tries} tries left"
    if tries == 5:
        image.arms = '|'
        image.body = '|'
        image.legs = '| '
    if tries == 4:
        image.arms = '|      |'
        image.legs = '| '
    if tries == 3:
        image.arms = '|     \\|'
        image.legs = '| '
    if tries == 2:
        image.arms = '|     \\|/'
        image.legs = '| '
    if tries == 1:
        image.arms = '|     \\|/'
        image.legs = '|     / '

    image = [value for _, value in image.__dict__.items()]
    image = '\t' + '\n\t'.join(image)
    return image
