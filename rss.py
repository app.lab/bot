import requests
import re
import persistence as db
import html

def patterns(feed):
    is_atom = '</feed>' in feed
    rss_item = '<item.*?>(.*?)</item>'
    rss_title = '<title.*?>(.*?)</title>'
    rss_link = '<link.*?>(.*?)</link>'
    atom_item = '<entry>(.*?)</entry>'
    atom_title = '<title.*?>(.*?)</title>'
    atom_link = '<link.*?href=\"(.*?)\".*?>'
    return atom_item if is_atom else rss_item, atom_title if is_atom else rss_title, atom_link if is_atom else rss_link

def remove_cdata(text):
    return text.strip().replace('<![CDATA[', '').replace(']]>', '')

def extract_title(url):
    feed = requests.get(url).text
    item, title, link = patterns(feed)
    titles = re.findall(title, feed, re.DOTALL)
    return '' if not titles else remove_cdata(titles[0])

async def update_rss(update_url=None):
    for row in db.list_rss():
        id, title, url = row
        if update_url and update_url != url:
            continue
        feed = requests.get(url).text
        item, title, link = patterns(feed)
        titles, links = [], []
        for entry in re.findall(item, feed, re.DOTALL):
            for e in re.findall(title, entry, re.DOTALL):
                titles.append(html.unescape(remove_cdata(e)))
            for e in re.findall(link, entry, re.DOTALL):
                links.append(e.strip())
        entries = zip([id for _ in titles], titles[::-1], links[::-1])
        db.add_entries(entries)

def latest():
    for row in db.list_rss():
        id, title, url = row
        entries = db.list_entries(id)
        if entries:
            eid, etitle, eurl, _, _ = entries[0]
            yield title, etitle, eurl
