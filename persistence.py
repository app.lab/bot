import os
import hashlib
import sqlite3 as sql
from b2sdk.v1 import *

filename = 'botdata.db'
filename_hash = None
bucket = os.environ['B2_BUCKET']

def b2_authorize():
    b2_api = B2Api(InMemoryAccountInfo())
    b2_api.authorize_account("production", os.environ['B2_KEY_ID'], os.environ['B2_KEY'])
    return b2_api

def b2_upload(bucket, local_file_name):
    new_filename_hash = hashsum(local_file_name)
    global filename_hash
    if new_filename_hash != filename_hash:
        api = b2_authorize()
        bucket = api.get_bucket_by_name(bucket)
        bucket.upload_local_file(
            local_file=local_file_name,
            file_name=local_file_name, #b2 file name
        )
        filename_hash = new_filename_hash
        print(f'successfully uploaded {local_file_name} into {bucket}')

def b2_download(bucket, file_name, local_file_name):
    api = b2_authorize()
    bucket = api.get_bucket_by_name(bucket)
    destination = DownloadDestLocalFile(local_file_name)
    bucket.download_file_by_name(file_name, destination)
    global filename_hash
    filename_hash = hashsum(file_name)

def hashsum(file_name):
    hashsum = None
    if os.path.exists(file_name):
        hash = hashlib.blake2b()
        with open(file_name, 'rb') as file:
            while True:
                chunk = file.read(hash.block_size)
                if not chunk:
                    break
                hash.update(chunk)
        hashsum = hash.hexdigest()
    return hashsum

def init():
    if not os.path.exists(filename):
        b2_download(bucket, filename, filename)
    with sql.connect(filename) as con:
        con.execute('create table if not exists note (id integer primary key, scope text, text text)')
        con.execute('create table if not exists rss (id integer primary key, url text, title text)')
        con.execute("""create table if not exists entries (
                            rss_id integer, url text unique, title text, ts timestamp default current_timestamp,
                            primary key(url) 
                            foreign key(rss_id) references rss(id))""")
        con.execute("""create table if not exists sub (
                            room_id integer, rss_id integer, last_update timestamp, 
                            foreign key(rss_id) references rss(id),
                            primary key(rss_id, room_id))""")
        con.execute('create table if not exists config (id integer primary key, key text, value text, ts timestamp default current_timestamp)')

        columns = [name for _, name, *_ in con.execute('pragma table_info(sub)')]
        if 'thread_id' not in columns:
            con.execute('alter table sub add column thread_id')

        has_rss_room_pk = ['primary key(rss_id, room_id)' in schema for _, schema in con.execute("select name, sql from sqlite_master where name = 'sub'")]
        if True in has_rss_room_pk:
            con.execute("""create table if not exists sub_copy (
                                room_id text, thread_id text, rss_id integer, last_update timestamp,
                                foreign key(rss_id) references rss(id),
                                primary key(rss_id, room_id, thread_id))""")
            con.execute('insert into sub_copy (room_id, rss_id, last_update) select room_id, rss_id, last_update from sub')
            con.execute('drop table sub')
            con.execute('alter table sub_copy rename to sub')

        con.commit()

def add_note(text, scope='default'):
    init()
    with sql.connect(filename) as con:
        con.execute("insert into note (scope, text) values (?, ?)", (scope, text))

def read_notes(scope='default'):
    init()
    with sql.connect(filename) as con:
        res = con.execute("select id, scope, text from note where scope = ? order by id desc", (scope,))
        notes = [(row[0], row[1], row[2]) for row in res]
    return notes

def latest_notes(last_id):
    init()
    with sql.connect(filename) as con:
        res = con.execute("select id, scope, text from note where id > ?", (last_id,))
        notes = [(row[0], row[1], row[2]) for row in res]
    return notes


def list_notes():
    init()
    with sql.connect(filename) as con:
        res = con.execute("select scope, count(*) from note group by scope")
        scopes = [(row[0], row[1]) for row in res]
    return scopes


def delete_note(id):
    init()
    with sql.connect(filename) as con:
        con.execute("delete from note where id = ?", (id,))

def add_rss(url, title):
    init()
    with sql.connect(filename) as con:
        con.execute("insert into rss (url, title) values (?, ?)", (url, title))
        cur = [row[0] for row in con.execute("select max(id) from rss where url = ? and title = ? limit 1", (url, title))]
        return cur[0]

def list_rss():
    init()
    with sql.connect(filename) as con:
        res = con.execute("select id, title, url from rss")
        rss = [(row[0], row[1], row[2]) for row in res]
    return rss

def delete_rss(id):
    init()
    with sql.connect(filename) as con:
        con.execute("delete from rss where id = ?", (id,))
        con.execute("delete from entries where rss_id = ?", (id,))

def add_entries(entries):
    init()
    with sql.connect(filename) as con:
        con.executemany("insert or ignore into entries (rss_id, title, url) values (?, ?, ?)", entries)

def list_entries(rss_id):
    init()
    with sql.connect(filename) as con:
        res = con.execute('select e.rowid, e.title, e.url, e.ts, rss.title as source from entries e, rss where e.rss_id = ? and e.rss_id == rss.id order by e.rowid desc limit 5', (rss_id,))
        return [(row[0], row[1], row[2], row[3], row[4]) for row in res]

def add_sub(room_id, rss_id, thread_id=None):
    init()
    with sql.connect(filename) as con:
        if thread_id:
            con.execute("insert or replace into sub (room_id, thread_id, rss_id) values (?, ?, ?)", (room_id, thread_id, rss_id))
        else:
            con.execute("insert or replace into sub (room_id, rss_id) values (?, ?)", (room_id, rss_id))

def del_sub(room_id, rss_id, thread_id=None):
    init()
    with sql.connect(filename) as con:
        if thread_id:
            con.execute("delete from sub where room_id = ? and rss_id = ? and thread_id = ?", (room_id, rss_id, thread_id))
        else:
            con.execute("delete from sub where room_id = ? and rss_id = ?", (room_id, rss_id))

def list_all_subs():
    init()
    with sql.connect(filename) as con:
        res = con.execute('select room_id, rss_id, last_update, thread_id from sub')
        return [(row[0], row[1], row[2], row[3]) for row in res]

def list_subs(room_id):
    init()
    with sql.connect(filename) as con:
        res = con.execute('select sub.rss_id, rss.title, rss.url from sub, rss where rss.id = sub.rss_id and room_id = ?', (room_id,))
        return [(row[0], row[1], row[2]) for row in res]

def update_sub(room_id, rss_id, thread_id=None):
    init()
    with sql.connect(filename) as con:
        if thread_id:
            con.execute('update sub set last_update = current_timestamp where room_id = ? and rss_id = ? and thread_id = ?', (room_id, rss_id, thread_id))
        else:
            con.execute('update sub set last_update = current_timestamp where room_id = ? and rss_id = ?', (room_id, rss_id))

def config(key=None, value=None):
    init()
    with sql.connect(filename) as con:
        if key and value:
            con.execute('insert or replace into config (key, value) values (?, ?)', (key, value))
            b2_upload(bucket, filename)
        else:
            res = con.execute('select key, value from config')
            return {k: v for k, v in res}
